## Pre-requisites

1. Onborded EC2 instance
2. installed Go , Docker ,Dcoker Compose,GIT, make
3. Register Git lab account
4. fork remote repo into https://gitlab.com/SD5027336/
5. Clone GiltLab repository with SSH in AWS -
   ssh-keygen -t rsa -C "dumbre.shobhana04@gmail.com" -b 4096
   cat ~/.ssh/id_rsa.pub
6. Settings/Repository/Deploy Keys and create a new key.
  

7. Executed all makefile command manually 

make dep      // pulled all dependecies
make build    // build binaries
make run      //test run
make docker_image   // build docker image 
make docker_testrun  //test docker container 
make docker_tar    // exports the docker image as a tarball

# Containerisation

Created dockerfile -   Searched golang image into docker hub , Copied code to dockerfile and made chnages accordingly added build and run commands followed makefile instruction 

executed and tested docker file , commited changes to remote repo

Containarization completed 

# Pipeline

1. gitlab-ci.yml 
 ran sample gitlab.yml file and modified file according to the requirement

2. installed and configured git lab runner 

Download the binary for your system
sudo curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64

Give it permission to execute
sudo chmod +x /usr/local/bin/gitlab-runner

Create a GitLab Runner user
sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash

Install and run as a service
sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
sudo gitlab-runner start

sudo gitlab-runner register --url https://gitlab.com/ --registration-token $REGISTRATION_TOKEN 

sudo usermod -aG docker gitlab-runner

3. Configued runner to pick jobs


# Environment

1. Created docker-compose.yml file
2. Executed Compose file manually

docker compose -f ./deployments/docker-compose.yml up -V -d

3. Executed thorugh CICD

CONTAINER ID   IMAGE                  COMMAND                 CREATED              STATUS          PORTS                                       NAMES
1fb6e2623484   devops/pinger:latest   "go run ./cmd/pinger"   About a minute ago   Up 18 seconds   0.0.0.0:8002->8000/tcp, :::8002->8000/tcp   deployments-pinger_test-1
7f68d9b8344c   devops/pinger:latest   "go run ./cmd/pinger"   About a minute ago   Up 19 seconds   0.0.0.0:8001->8000/tcp, :::8001->8000/tcp   deployments-pinger_host-1

pinging host container to test container 
docker exec -it deployments-pinger_host-1 bin/pinger pinger_test

# Documentation
make verify_readme


